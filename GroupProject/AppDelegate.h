//
//  AppDelegate.h
//  GroupProject
//
//  Created by Carley,Ryan J on 10/5/16.
//  Copyright © 2016 Carley,Ryan J. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

