//
//  main.m
//  GroupProject
//
//  Created by Carley,Ryan J on 10/5/16.
//  Copyright © 2016 Carley,Ryan J. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
